//
//  AppConstants.swift
//  MyTravelHelper
//
//  Created by Satish on 11/03/19.
//  Copyright © 2019 Sample. All rights reserved.
//

import Foundation

let PROGRESS_INDICATOR_VIEW_TAG: Int = 10
let kFavSourceStation = "favouriteSource"
let kFavDestinationStation = "favouriteDestination"

enum ErrorMessage: Error {
    case network
    case noArrivingFromSource
    case noTrainsFound
    case invalidStationName
    case tryAgain

    var message: String {
        switch self {
        case .network:
            return "Please Check you internet connection and try again"
        case .noArrivingFromSource:
            return "Sorry No trains arriving source station in another 90 mins"
        case .noTrainsFound:
            return "Sorry No trains Found from source to destination in another 90 mins"
        case .invalidStationName:
            return "Invalid Source or Destination Station names Please Check"
        case .tryAgain:
            return "Please try again!"
        }
    }
}
