//
//  TrainInfoCell.swift
//  MyTravelHelper
//
//  Created by Satish on 11/03/19.
//  Copyright © 2019 Sample. All rights reserved.
//

import UIKit

class TrainInfoCell: UITableViewCell {
    @IBOutlet weak var destinationTimeLabel: UILabel!
    @IBOutlet weak var sourceTimeLabel: UILabel!
    @IBOutlet weak var destinationInfoLabel: UILabel!
    @IBOutlet weak var souceInfoLabel: UILabel!
    @IBOutlet weak var trainCode: UILabel!

    var data: StationTrain? {
        didSet {
            trainCode.text = data?.trainCode
            souceInfoLabel.text = data?.stationFullName
            sourceTimeLabel.text = data?.expDeparture
            if let _destinationDetails = data?.destinationDetails {
                destinationInfoLabel.text = _destinationDetails.locationFullName
                destinationTimeLabel.text = _destinationDetails.expDeparture
            }
        }
    }
}
